// Load Module Dependencies

subscriptionRouter  = require('./subscribe')


module.exports = function appRouter(app) {
	// User Routes
	app.use('/subscription', subscriptionRouter);




	app.get('/', function getRoot(req, res){
		res.json({
			message: 'Sheba Books API subscription service'
		})
	});

}