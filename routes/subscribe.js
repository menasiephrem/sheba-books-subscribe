// Load Module Dependencies
var express = require('express');

var subscribeController = require('../controllers/subscribe');

var router = express.Router();

/**
 * @api {POST} /subscription/ Create Subscription
 * @apiName CreatSubscription
 * @apiGroup Subscription
 *
 * @apiDescription Creates a new Subscription.
  * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer HrAuoeKL2NtpG9tRKkZ/J0vjDZu1psZneK8bKpdO6Ps8x5BXFsA9oMnSD4GCnxk9oWD2u3cmokj7"
*     }
 *
 * @apiParam {String} plan_id the id of the plan of the user subscribed to
 * @apiParam {String} user_id the id of the user that is subscribing to the plan 


 * @apiParamExample Request Example:
{
  "plan_id":               "5a7eaaa708644b31db10f109",
  "user_id":               "5a7e869e8b822916071d7ce6"
}
 *
 *
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 201 Created
  {
            "_id": "5a7ed844c0e9ca53e7ea9327",
            "last_modified": "2018-02-10T11:51:37.910Z",
            "date_created": "2018-02-10T11:32:20.079Z",
            "plan_id": {
                "_id": "5a7eb05429edd235c2e6edf5",
                "last_modified": "2018-02-10T08:41:56.299Z",
                "name": "Legend",
                "description": "A Legend plan to subscribe"
            },
            "user_id": {
                "_id": "5a7e869e8b822916071d7ce6",
                "last_modified": "2018-02-10T08:22:06.199Z",
                "date_created": "2018-02-10T05:43:59.052Z",
                "username": "mamo@sheba.com",
                "role": "client",
                "isActive": false
            },
            "__v": 0,
            "isActive": true
        }
 */
router.post('/', subscribeController.creatSubscription);

/**
 * @api {GET} /subscription/ Get Subscription By Pagination
 * @apiName GetSubscriptionPagination 
 * @apiGroup Subscription
 *
 * @apiDescription Get all Subscription by pagination

  * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer HrAuoeKL2NtpG9tRKkZ/J0vjDZu1psZneK8bKpdO6Ps8x5BXFsA9oMnSD4GCnxk9oWD2u3cmokj7"
*     }
 *
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
 {
    "page": 1,
    "total_docs": 2,
    "total_pages": 1,
    "per_page": 10,
    "docs": [
        {
            "_id": "5a7edf46f92a495f85f819c7",
            "last_modified": "2018-02-10T12:02:14.208Z",
            "date_created": "2018-02-10T12:02:14.208Z",
            "plan_id": {
                "_id": "5a7eaaa708644b31db10f109",
                "last_modified": "2018-02-10T08:17:43.338Z",
                "name": "Starter",
                "description": "A starter plan to subscribe"
            },
            "user_id": {
                "_id": "5a7e869e8b822916071d7ce6",
                "last_modified": "2018-02-10T08:22:06.199Z",
                "date_created": "2018-02-10T05:43:59.052Z",
                "username": "mamo@sheba.com",
                "role": "client",
                "isActive": false
            },
            "__v": 0,
            "isActive": true
        },
        {
            "_id": "5a7edf88f92a495f85f819c8",
            "last_modified": "2018-02-10T12:03:20.810Z",
            "date_created": "2018-02-10T12:03:20.810Z",
            "plan_id": {
                "_id": "5a7eaa8408644b31db10f108",
                "last_modified": "2018-02-10T08:56:07.177Z",
                "name": "Economy",
                "description": "An Economy plan to subscribe"
            },
            "user_id": {
                "_id": "5a7e869e8b822916071d7ce6",
                "last_modified": "2018-02-10T08:22:06.199Z",
                "date_created": "2018-02-10T05:43:59.052Z",
                "username": "mamo@sheba.com",
                "role": "client",
                "isActive": false
            },
            "__v": 0,
            "isActive": true
        }
    ]
}

 */
router.get('/',subscribeController.getCollectionByPagination);

/**
 * @api {GET} /subscription/:id Get a single subscription
 * @apiName GetSubscription 
 * @apiGroup Subscription
 *
 * @apiDescription Get all Get a single subscription

  * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer HrAuoeKL2NtpG9tRKkZ/J0vjDZu1psZneK8bKpdO6Ps8x5BXFsA9oMnSD4GCnxk9oWD2u3cmokj7"
*     }
 *
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
 {
            "_id": "5a7edf88f92a495f85f819c8",
            "last_modified": "2018-02-10T12:03:20.810Z",
            "date_created": "2018-02-10T12:03:20.810Z",
            "plan_id": {
                "_id": "5a7eaa8408644b31db10f108",
                "last_modified": "2018-02-10T08:56:07.177Z",
                "name": "Economy",
                "description": "An Economy plan to subscribe"
            },
            "user_id": {
                "_id": "5a7e869e8b822916071d7ce6",
                "last_modified": "2018-02-10T08:22:06.199Z",
                "date_created": "2018-02-10T05:43:59.052Z",
                "username": "mamo@sheba.com",
                "role": "client",
                "isActive": false
            },
            "__v": 0,
            "isActive": true
        }
 */
router.get('/:id',subscribeController.getSubscription);

/**
 * @api {GET} /Subscriptiones/:id Get a Single Subscription
 * @apiName GetSubscription
 * @apiGroup Subscription
 *
 * @apiDescription Get a single Subscription

  * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer HrAuoeKL2NtpG9tRKkZ/J0vjDZu1psZneK8bKpdO6Ps8x5BXFsA9oMnSD4GCnxk9oWD2u3cmokj7"
*     }
 *
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
{
    "_id": "5a75ba9cb39f7e3699ef85cb",
    "last_modified": "2018-02-03T13:35:24.583Z",
    "percent_of_Subscription": 75,
    "communicating": true,
    "profile2": {
        "_id": "5a75ada536cdf2267a922bed",
        "last_modified": "2018-02-03T12:40:05.625Z",
        "date_created": "2018-02-03T12:40:05.625Z",
        "email": "mamo@gebeya.com",
        "user": "5a75ada536cdf2267a922bec",
        "phone_number": 251911123456,
        "first_name": "Dor",
        "last_name": "Mamo",
        "picture": ""
    },
    "profile1": {
        "_id": "5a75b167af44b3299cb3f131",
        "last_modified": "2018-02-03T12:56:07.463Z",
        "date_created": "2018-02-03T12:56:07.463Z",
        "email": "mamit@gebeya.com",
        "user": "5a75b167af44b3299cb3f130",
        "picture": ""
    }
}
 */
router.get('/:id', subscribeController.getSubscription);
/**
 * @api {PUT} /subscription/:id update a Subscription
 * @apiName UpdateSubscription
 * @apiGroup Subscription
 *
 * @apiDescription Update a single Subscription

  * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer HrAuoeKL2NtpG9tRKkZ/J0vjDZu1psZneK8bKpdO6Ps8x5BXFsA9oMnSD4GCnxk9oWD2u3cmokj7"
*     }
 *
 * @apiParam {String} plan_id the id of the plan of the user subscribed to
 * @apiParam {String} user_id the id of the user that is subscribing to the plan
 * @apiParamExample Request Example:
{
  "plan_id":               "5a7eb05429edd235c2e6edf5",
  "user_id":               "5a7e869e8b822916071d7ce6"
}

 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
 {
    "_id": "5a7edf46f92a495f85f819c7",
    "last_modified": "2018-02-10T12:17:42.371Z",
    "date_created": "2018-02-10T12:02:14.208Z",
    "plan_id": {
        "_id": "5a7eb05429edd235c2e6edf5",
        "last_modified": "2018-02-10T08:41:56.299Z",
        "name": "Legend",
        "description": "A Legend plan to subscribe"
    },
    "user_id": {
        "_id": "5a7e869e8b822916071d7ce6",
        "last_modified": "2018-02-10T08:22:06.199Z",
        "date_created": "2018-02-10T05:43:59.052Z",
        "username": "mamo@sheba.com",
        "role": "client",
        "isActive": false
    },
    "isActive": true
}
 */
router.put('/:id',subscribeController.updateSubscription);

/**
 * @api {DELETE} /subscription/:id Delete a Single Subscription
 * @apiName DeleteSubscription
 * @apiGroup Subscription
 *
 * @apiDescription Delete a single Subscription

  * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer HrAuoeKL2NtpG9tRKkZ/J0vjDZu1psZneK8bKpdO6Ps8x5BXFsA9oMnSD4GCnxk9oWD2u3cmokj7"
*     }
 *
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
 {
    "_id": "5a7edf46f92a495f85f819c7",
    "last_modified": "2018-02-10T12:17:42.371Z",
    "date_created": "2018-02-10T12:02:14.208Z",
    "plan_id": {
        "_id": "5a7eb05429edd235c2e6edf5",
        "last_modified": "2018-02-10T08:41:56.299Z",
        "name": "Legend",
        "description": "A Legend plan to subscribe"
    },
    "user_id": {
        "_id": "5a7e869e8b822916071d7ce6",
        "last_modified": "2018-02-10T08:22:06.199Z",
        "date_created": "2018-02-10T05:43:59.052Z",
        "username": "mamo@sheba.com",
        "role": "client",
        "isActive": false
    },
    "isActive": true
}
 */
router.delete('/:id', subscribeController.remove);

/**
 * @api {GET} /subscription/user/:id Get a single subscription for one user
 * @apiName GetSubscriptionForAUser 
 * @apiGroup Subscription
 *
 * @apiDescription Get the subscription a user belongs to

  * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer HrAuoeKL2NtpG9tRKkZ/J0vjDZu1psZneK8bKpdO6Ps8x5BXFsA9oMnSD4GCnxk9oWD2u3cmokj7"
*     }
 *
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
 {
            "_id": "5a7edf88f92a495f85f819c8",
            "last_modified": "2018-02-10T12:03:20.810Z",
            "date_created": "2018-02-10T12:03:20.810Z",
            "plan_id": {
                "_id": "5a7eaa8408644b31db10f108",
                "last_modified": "2018-02-10T08:56:07.177Z",
                "name": "Economy",
                "description": "An Economy plan to subscribe"
            },
            "user_id": {
                "_id": "5a7e869e8b822916071d7ce6",
                "last_modified": "2018-02-10T08:22:06.199Z",
                "date_created": "2018-02-10T05:43:59.052Z",
                "username": "mamo@sheba.com",
                "role": "client",
                "isActive": false
            },
            "__v": 0,
            "isActive": true
        }
 */
router.get('/user/:id',subscribeController.getUserSubscription);

/**
 * @api {GET} /subscription/plan/:id Get a all subscription for one plan
 * @apiName GetSubscriptionForAPlan
 * @apiGroup Subscription
 *
 * @apiDescription Get the subscriptions that belongs to a singel plan

  * @apiHeader  {String} Authorization  Bearer + {token} a required header for all the API end-points. The token can be found form the login end point
* 
* @apiHeaderExample {json} Header-Example:
*     {
*       "Authorization": "Bearer HrAuoeKL2NtpG9tRKkZ/J0vjDZu1psZneK8bKpdO6Ps8x5BXFsA9oMnSD4GCnxk9oWD2u3cmokj7"
*     }
 *
 * @apiSuccessExample Response Example:
 *  HTTP/1.1 200 OK
{
    "page": 1,
    "total_docs": 1,
    "total_pages": 1,
    "per_page": 10,
    "docs": [
        {
            "_id": "5a7ef7c6c5e90075530bb9a3",
            "last_modified": "2018-02-10T13:47:37.916Z",
            "date_created": "2018-02-10T13:46:46.284Z",
            "plan_id": {
                "_id": "5a7eaaa708644b31db10f109",
                "last_modified": "2018-02-10T08:17:43.338Z",
                "name": "Starter",
                "description": "A starter plan to subscribe"
            },
            "user_id": {
                "_id": "5a7e869e8b822916071d7ce6",
                "last_modified": "2018-02-10T08:22:06.199Z",
                "date_created": "2018-02-10T05:43:59.052Z",
                "username": "mamo@sheba.com",
                "role": "client",
                "isActive": false
            },
            "__v": 0,
            "isActive": true
        }
    ]
}
 */
router.get('/plan/:id',subscribeController.getPlanSubscription);

module.exports = router