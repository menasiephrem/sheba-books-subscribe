/**
 * Load Module Dependencies Subscription 
 */

var EventEmitter    = require('events').EventEmitter;
var debug 			= require('debug')('api:Subscription-controller');
var moment    		= require('moment');
var _              = require('lodash');

var SubscriptionDal  		= require('../dal/subscribe');


/**
 * Create Subscription
 * 
 * @desc Create Subscription and Subscription Type then Respond with
 *       created Subscription
 * 
 * @throws {Subscription_CREATION_ERROR}
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
// POST Subscriptions
exports.creatSubscription = function creatSubscription(req, res, next){
	debug('create Subscription');
	var body        = req.body;
	var workflow = new EventEmitter();




	workflow.on('check_subscripiton', function(){
		debug('check if the user already subscribed to the plan');


		var qq = [];

		var plan_id  =       { plan_id: body.plan_id};
		var user_id  = 		 { user_id: body.user_id};

		qq.push(plan_id);
		qq.push(user_id);

		var query = {$and : qq};

		SubscriptionDal.get(query, function (err, docs){
			if(err) {
					res.status(500);
					res.json({
						status: 500,
						type: 'SUBSCRIPTION_COLLECTION_ERROR',
						message: err.message
					});
					return;
			}
			if(!_.isEmpty(docs)){
				
					res.status(403);
					res.json({
						status: 403,
						type: 'USER_ALREADY_SUBSRIBED',
						message: "User already subscribed to this plan"
					});	
					return;
			}else{
				workflow.emit('check_type');
			}
		});
	});

workflow.on('check_type', function(){
	debug('check the type of plan')

	SubscriptionDal.get({user_id : body.user_id}, function getCB(err, Subscription){
		debug('Subscription length');
		if(err){
			res.status(500);
			res.json({
				status: 500,
				type: 'GET_Subscription_ERROR',
				message: err.message
			});
			return;
		}
		if(!_.isEmpty(Subscription)){
			workflow.emit('update_user_plan',Subscription);
		}else
		{
			workflow.emit('create_a_new_subscription');
		}
	});
});

workflow.on('update_user_plan',function(Subscription){
	debug('update the subscribed plan');
	var bb = {};
	var now = moment().toISOString();
	bb.last_modified = now;
	bb.plan_id = body.plan_id;

	SubscriptionDal.update({_id : Subscription._id}, bb, function updateCB(err, Subscription){
		if(err){
			res.status(500);
			res.json({
				status: 500,
				type: 'UPDATE_Subscription_ERROR',
				message: err.message
			});
			return;
		}
		res.json(Subscription || {} );
	});
});

workflow.on('create_a_new_subscription', function(Subscription){
	debug('create a new Subscription');
	SubscriptionDal.create(body, function createCB(err, Subscription){
		if(err){
			res.status(500);
			res.json({
				status: 500,
				type: 'CREATE_Subscription_ERROR',
				message: err.message
			});
			return;
		}
		res.status(201);
		res.json(Subscription);
	});
});

workflow.emit('check_subscripiton');
};

/**
 * GET all Subscriptions
 * 
 * @desc Get all Subscriptions using pagination the parmetrs are pass as via the url 
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
exports.getCollectionByPagination = function getCollectionByPagination(req, res, next){
	debug('Get Subscriptions Collection By Pagination');

	var query = req.query.query || {};
	var qs = req.query;

	SubscriptionDal.getCollectionByPagination(query, qs, function (err, docs){
		if(err) {
				res.status(500);
				res.json({
					status: 500,
					type: 'Subscription_COLLECTION_PAGINATED_ERROR',
					message: err.message
				});
				return;
		}

		res.json(docs);
	})
}

/**
 * Delete Subscription
 * 
 * @desc Delete Subscription and Corresponding Subscription from the DB
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
 exports.remove = function removeSubscription(req, res, next) {
 	debug('Remove Subscription: ', req.params.id);

 	var query = {
 		_id: req.params.id
 	};
 	
 	SubscriptionDal.delete(query, function deleteSubscription(err, Subscription){
 		if(err){
 		  res.status(500);
      res.json({
        status: 500,
        type: 'Subscription_REMOVE_ERROR',
        message: err.message
      });
      return;
 		}

    if(!Subscription._id) {
      res.status(400);
      res.json({
          status: 400,
          type: 'Subscription_REMOVE_ERROR',
          message: 'Subscription Does not Exist!!'
      });
      return;
    }
    		res.json(Subscription ||{});
 	})
 };


/**
 * GET a Subscription
 * 
 * @desc Get a single Subscription
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
exports.getSubscription = function getSubscription(req, res, next){
	var Id = req.params.id;
	SubscriptionDal.get({_id: Id}, function getCB(err, Subscription){
		if(err){
			res.status(500);
			res.json({
				status: 500,
				type: 'GET_Subscription_ERROR',
				message: err.message
			});
			return;
		}
		res.json(Subscription ||{});
	});

};

/**
 * Update a Subscription
 * 
 * @desc  Update a single Subscription
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
exports.updateSubscription = function updateSubscription(req, res, next){
	
	var Id = req.params.id;
	var body = req.body;
	var now = moment().toISOString();
	body.last_modified = now
	SubscriptionDal.update({_id : Id}, body, function updateCB(err, Subscription){

		if(err){
			res.status(500);
			res.json({
				status: 500,
				type: 'UPDATE_Subscription_ERROR',
				message: err.message
			});
			return;
		}
		res.json(Subscription || {} );
	});
};


/**
 * GET a Subscription for one user
 * 
 * @desc  Get a single Subscription for one user
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
exports.getUserSubscription = function getUserSubscription(req, res, next){
	debug('get subscription for user',req.params.id);
	
	var Id = req.params.id;

	SubscriptionDal.get({user_id : Id}, function getCB(err, Subscription){
		if(err){
			res.status(500);
			res.json({
				status: 500,
				type: 'GET_Subscription_ERROR',
				message: err.message
			});
			return;
		}
		res.json(Subscription || {});
	});
};

/**
 * GET a Subscription for a single plan
 * 
 * @desc  return a paginated collection of all subscriptions for one plan
 *
 * @param {Object} req HTTP Request Object
 * @param {Object} res HTTP Response Object
 * @param {Function} next Middleware Dispatcher
 */
exports.getPlanSubscription = function getPlanSubscription(req, res, next){
	debug('get subscription for plan ',req.params.id);
	
	var Id = req.params.id;

	var query = req.query.query || {};
	var qs = req.query;

	SubscriptionDal.getCollectionByPagination({plan_id: Id}, qs, function (err, docs){
		if(err) {
				res.status(500);
				res.json({
					status: 500,
					type: 'Subscription_COLLECTION_PAGINATED_ERROR',
					message: err.message
				});
				return;
		}

		res.json(docs);
	})
};


// Noop Method
exports.noop = function noop(req, res, next) {
	res.json({
		message: 'To be Implemented'
	});
}