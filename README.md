# Sheba Books: Subscription-service 
-Menasi Ephrem menasiephrem@gmail.com

This is Sheba books subscripiotn-service API: it is responsible for the Manipulation of the subscribe model. 

The Api documentaion is built by apiDoc and can be accessed by using the following link [documentation link](https://menasiephrem.github.io/sheba/) 

Here is a link to postman collection that has all the end points [postman collection link](https://www.getpostman.com/collections/996d93dbfb71d44d0a9f)  

### Installation

Sheba Books:Subscription-service requires [Node.js](https://nodejs.org/) v4+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd sheba-subscribtion
$ npm install 
$ npm start
```

For testing...

```sh
$ npm test
```
NB. every end point in Subscription service needs an authorization token to work, so in order to have a successful test Auth-service and subscription-service need to be cloned in the same root folder.

Here is the link to the API baseurl (the NON-Docker deployment)  [https://sheba-subscibe.herokuapp.com/ ](https://sheba-subscibe.herokuapp.com/ )

A link to the API baseurl (the Docker deployment) [https://sheba-subscibe-docker.herokuapp.com/ ](https://sheba-subscibe-docker.herokuapp.com/ )


