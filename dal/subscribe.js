// Access Layer for subscribe Data.

/**
 * Load Module Dependencies.
 */
var debug   = require('debug')('api:dal-subscribe');
var moment  = require('moment');
var _       = require('lodash');

var subscribe        = require('../models/subscribe');
var user             = require('../models/user');
var plan             = require('../models/plan');


var returnFields = subscribe.whitelist;
var population = [{
  path: 'user_id',
  select: user.whitelist
},{
  path: 'plan_id',
  select: plan.whitelist
}];

/**
 * create a new subscribe.
 *
 * @desc  creates a new subscribe and saves them
 *        in the database
 *
 * @param {Object}  subscribeData  Data for the subscribe to create
 * @param {Function} cb       Callback for once saving is complete
 */
exports.create = function create(subscribeData, cb) {
  debug('creating a new subscribe');

  // Create subscribe if is new.
  var subscribeModel  = new subscribe(subscribeData);

  subscribeModel.save(function savesubscribe(err, data) {
    if (err) {
      return cb(err);
    }

    exports.get({ _id: data._id }, function (err, subscribe) {
      if(err) {
        return cb(err);
      }

      cb(null, subscribe);

    });

  });

};

/**
 * delete a subscribe
 *
 * @desc  delete data of the subscribe with the given
 *        id
 *
 * @param {Object}  query   Query Object
 * @param {Function} cb Callback for once delete is complete
 */
exports.delete = function deletesubscribe(query, cb) {
  debug('deleting subscribe: ', query);

  subscribe
    .findOne(query, returnFields)
    .populate(population)
    .exec(function deletesubscribe(err, subscribe) {
      if (err) {
        return cb(err);
      }

      if(!subscribe) {
        return cb(null, {});
      }

      subscribe.remove(function(err) {
        if(err) {
          return cb(err);
        }

        cb(null, subscribe);

      });

    });
};

/**
 * update a subscribe
 *
 * @desc  update data of the subscribe with the given
 *        id
 *
 * @param {Object} query Query object
 * @param {Object} updates  Update data
 * @param {Function} cb Callback for once update is complete
 */
exports.update = function update(query, updates,  cb) {
  debug('updating subscribe: ', query);

  var now = moment().toISOString();
  var opts = {
    'new': true,
    select: returnFields
  };

  subscribe
    .findOneAndUpdate(query, updates, opts)
    .populate(population)
    .exec(function updatesubscribe(err, subscribe) {
      if(err) {
        return cb(err);
      }

      cb(null, subscribe || {});
    });
};

/**
 * get a subscribe.
 *
 * @desc get a subscribe with the given id from db
 *
 * @param {Object} query Query Object
 * @param {Function} cb Callback for once fetch is complete
 */
exports.get = function get(query, toFilter, cb) {
  debug('getting subscribe ', query);

  if(arguments.length < 3) {
    cb = toFilter;
    toFilter = true;
  }

  subscribe
    .findOne(query, toFilter ? returnFields : null)
    .populate(population)
    .exec(function(err, subscribe) {
      if(err) {
        return cb(err);
      }

      cb(null, subscribe || {});
    });
};

/**
 * get a collection of subscribes
 *
 * @desc get a collection of subscribes from db
 *
 * @param {Object} query Query Object
 * @param {Function} cb Callback for once fetch is complete
 */
exports.getCollection = function getCollection(query, qs, cb) {
  debug('fetching a collection of subscribes');

  cb(null,
     subscribe
      .find(query, returnFields)
      .populate(population)
      .stream({ transform: JSON.stringify }));

};

/**
 * get a collection of subscribes using pagination
 *
 * @desc get a collection of subscribes from db
 *
 * @param {Object} query Query Object
 * @param {Function} cb Callback for once fetch is complete
 */
exports.getCollectionByPagination = function getCollection(query, qs, cb) {
  debug('fetching a collection of subscribes');

  var opts = {
    fields: returnFields,
    sort: qs.sort || {},
    populate: population,
    page: Number(qs.page) || 1,
    limit: Number(qs.per_page) || 10
  };

  subscribe.paginate(query, opts, function (err, data){
    if(err){
      return cb(err);
    }

    var response = {
      page: data.page,
      total_docs: data.total,
      total_pages: data.pages,
      per_page: data.limit,
      docs: data.docs
    };

    cb(null, response);

  });

};


