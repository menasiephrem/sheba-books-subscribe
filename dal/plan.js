// Access Layer for plan Data.

/**
 * Load Module Dependencies.
 */
var debug   = require('debug')('api:dal-plan');
var moment  = require('moment');
var _       = require('lodash');


var plan             = require('../models/plan');


var returnFields = plan.whitelist;
var population = [];

/**
 * create a new plan.
 *
 * @desc  creates a new plan and saves them
 *        in the database
 *
 * @param {Object}  planData  Data for the plan to create
 * @param {Function} cb       Callback for once saving is complete
 */
exports.create = function create(planData, cb) {
  debug('creating a new plan');

  // Create plan if is new.
  var planModel  = new plan(planData);

  planModel.save(function saveplan(err, data) {
    if (err) {
      return cb(err);
    }

    exports.get({ _id: data._id }, function (err, plan) {
      if(err) {
        return cb(err);
      }

      cb(null, plan);

    });

  });

};

/**
 * delete a plan
 *
 * @desc  delete data of the plan with the given
 *        id
 *
 * @param {Object}  query   Query Object
 * @param {Function} cb Callback for once delete is complete
 */
exports.delete = function deleteplan(query, cb) {
  debug('deleting plan: ', query);

  plan
    .findOne(query, returnFields)
    .populate(population)
    .exec(function deleteplan(err, plan) {
      if (err) {
        return cb(err);
      }

      if(!plan) {
        return cb(null, {});
      }

      plan.remove(function(err) {
        if(err) {
          return cb(err);
        }

        cb(null, plan);

      });

    });
};

/**
 * update a plan
 *
 * @desc  update data of the plan with the given
 *        id
 *
 * @param {Object} query Query object
 * @param {Object} updates  Update data
 * @param {Function} cb Callback for once update is complete
 */
exports.update = function update(query, updates,  cb) {
  debug('updating plan: ', query);

  var now = moment().toISOString();
  var opts = {
    'new': true,
    select: returnFields
  };

  plan
    .findOneAndUpdate(query, updates, opts)
    .populate(population)
    .exec(function updateplan(err, plan) {
      if(err) {
        return cb(err);
      }

      cb(null, plan || {});
    });
};

/**
 * get a plan.
 *
 * @desc get a plan with the given id from db
 *
 * @param {Object} query Query Object
 * @param {Function} cb Callback for once fetch is complete
 */
exports.get = function get(query, toFilter, cb) {
  debug('getting plan ', query);

  if(arguments.length < 3) {
    cb = toFilter;
    toFilter = true;
  }

  plan
    .findOne(query, toFilter ? returnFields : null)
    .populate(population)
    .exec(function(err, plan) {
      if(err) {
        return cb(err);
      }

      cb(null, plan || {});
    });
};

/**
 * get a collection of plans
 *
 * @desc get a collection of plans from db
 *
 * @param {Object} query Query Object
 * @param {Function} cb Callback for once fetch is complete
 */
exports.getCollection = function getCollection(query, qs, cb) {
  debug('fetching a collection of plans');

  cb(null,
     plan
      .find(query, returnFields)
      .populate(population)
      .stream({ transform: JSON.stringify }));

};

/**
 * get a collection of plans using pagination
 *
 * @desc get a collection of plans from db
 *
 * @param {Object} query Query Object
 * @param {Function} cb Callback for once fetch is complete
 */
exports.getCollectionByPagination = function getCollection(query, qs, cb) {
  debug('fetching a collection of plans');

  var opts = {
    fields: returnFields,
    sort: qs.sort || {},
    populate: population,
    page: Number(qs.page) || 1,
    limit: Number(qs.per_page) || 10
  };

  plan.paginate(query, opts, function (err, data){
    if(err){
      return cb(err);
    }

    var response = {
      page: data.page,
      total_docs: data.total,
      total_pages: data.pages,
      per_page: data.limit,
      docs: data.docs
    };

    cb(null, response);

  });

};


