// Load Module Dependencies

module.exports = {
	// HTTP PORT
	HTTP_PORT: process.env.PORT||4800,

	// DB Connection URL
	MONGODB_URL: 'mongodb://localhost/sheba-book',

	TOKEN_LENGTH: 57,

	CORS_OPTS: {
		    origin: '*',
		    methods: 'GET,POST,PUT,DELETE,OPTIONS',
		    allowedHeaders: 'Origin,X-Requested-With,Content-Type,Accept,Authorization'
  	},
};