var chai 	 = require('chai');
var request  = require('supertest');
var app 	 = require('../app');
var appAuth  = require('../../sheba-auth/app');


var userFix = require('./fixture/user');
var planFix = require('./fixture/plan');
var expect = chai.expect;

var userId   = null;
var authToke = '';
var planId   = null;
var subId	 = null;

describe('User Signup', function(){
	describe('POST /users/signup', function(){
		it('should signup a new user', function(done){
			 request(appAuth)
			  .post('/users/signup')
			  .send(userFix)
			  .set('Content-Type', 'application/json')
			  .end(function (err, res){
			  	if(err){
			  		return done(err);
			  	}

			  	if(res.status !== 201){
			  		var _err = JSON.parse(res.text);
			  		return done(new Error(_err.message));
			  	}

			  	var user = res.body;

			  	expect(user._id).to.be.a('string');
			  	expect(user.isActive).to.equal(true);
			  	expect(user.username).to.equal(userFix.email)
			  	userId = user._id;
			  	done();
			  });
		});
	});
});

describe('User Log in', function(){
	var loginInfo = {
		username : userFix.email,
		password : userFix.password
	};

	describe('POST /users/login', function(){
		it('should login a user', function(done){
			request(appAuth)
			  .post('/users/login')
			  .send(loginInfo)
			  .set('Content-Type', 'application/json')
			  .end(function (err, res){
	            if(err){
	              return done(err);
	            }

	            if(res.status !== 200) {
	              var _err = JSON.parse(res.text);
	              return done(new Error(_err.message));
	            }

	            var data = res.body;
	            
	            expect(data.token._id).to.be.a('string');
	            expect(data.token.value).to.be.a('string')
	            expect(data.token.revoked).to.equal(false);

	            authToken = data.token.value;

	            done();


			  });
		});		
	});

});

describe('Create Plan', function(){
	describe('POST /plans/', function(){
		it('should create a new plan', function(done){
			 request(appAuth)
			  .post('/plans/')
			  .send(planFix)
			  .set('Content-Type', 'application/json')
			  .set('Authorization', 'Bearer ' + authToken)
			  .end(function (err, res){
			  	if(err){
			  		return done(err);
			  	}

			  	if(res.status !== 201){
			  		var _err = JSON.parse(res.text);
			  		return done(new Error(_err.message));
			  	}

			  	var plan = res.body;

			  	expect(plan._id).to.be.a('string');
			  	expect(plan.name).to.equal(planFix.name)
			  	expect(plan.description).to.equal(planFix.description)
			  	planId = plan._id;
			  	done();
			  });
		});
	});
});


describe('Create Subscription', function(){
	describe('POST /subscription/', function(){
		it('should create a new subscription', function(done){
			 request(app)
			  .post('/subscription/')
			  .send({plan_id: planId, user_id: userId})
			  .set('Content-Type', 'application/json')
			  .set('Authorization', 'Bearer ' + authToken)
			  .end(function (err, res){
			  	if(err){
			  		return done(err);
			  	}

			  	if(res.status !== 201){
			  		var _err = JSON.parse(res.text);
			  		return done(new Error(_err.message));
			  	}

			  	var body = res.body;

			  	expect(body._id).to.be.a('string');
			  	expect(body.plan_id).to.be.a('Object');
			  	expect(body.user_id).to.be.a('Object');
			  	expect(body.plan_id._id).to.be.a('string');
			  	expect(body.user_id._id).to.be.a('string');
			  	expect(body.isActive).to.equal(true)
			  	subId = body._id;
			  	done();
			  });
		});
	});
});

describe('Create Subscription With same user and plan', function(){
	describe('POST /subscription/', function(){
		it('should return an error message', function(done){
			 request(app)
			  .post('/subscription/')
			  .send({plan_id: planId, user_id: userId})
			  .set('Content-Type', 'application/json')
			  .set('Authorization', 'Bearer ' + authToken)
			  .end(function (err, res){
			  	if(err){
			  		return done(err);
			  	}

			  	if(res.status !== 403){
			  		var _err = JSON.parse(res.text);
			  		return done(new Error(_err.message));
			  	}

			  	var body = res.body;

			  
			  	expect(body.type).to.be.equal('USER_ALREADY_SUBSRIBED');
			  	expect(body.message).to.be.equal('User already subscribed to this plan');
			  	done();
			  });
		});
	});
});

describe('Get a Single Subscription', function(){
	describe('GET /subscription/:id',function(){
		it('It should return one single Subscription',function(done){
		  request(app)
		    .get('/subscription/'+subId)
		    .set('Content-Type', 'application/json')
		    .set('Authorization', 'Bearer ' + authToken)
		    .end(function(err, res){
			    if(err){
	              return done(err);
	            }

	            if(res.status !== 200) {
	              var _err = JSON.parse(res.text);
	              return done(new Error(_err.message));
	            }

	            var body = res.body;

		  	    expect(body._id).to.be.a('string');
			  	expect(body.plan_id).to.be.a('Object');
			  	expect(body.user_id).to.be.a('Object');
			  	expect(body.plan_id._id).to.be.a('string');
			  	expect(body.user_id._id).to.be.a('string');
			  	expect(body.plan_id._id).to.be.equal(planId);
			  	expect(body.user_id._id).to.be.equal(userId);

	            done();
		    });
		});
	});
});

describe('Get Subscription Collection', function(){
	describe('GET /subscription/',function(){
		it('It should return collection of Subscription by pagination',function(done){
		  request(app)
		    .get('/subscription/')
		    .set('Content-Type', 'application/json')
		    .set('Authorization', 'Bearer ' + authToken)
		    .end(function(err, res){
			    if(err){
	              return done(err);
	            }

	            if(res.status !== 200) {
	              var _err = JSON.parse(res.text);
	              return done(new Error(_err.message));
	            }

	            var data = res.body;

	            expect(data.page).to.be.a('number');
	            expect(data.total_docs).to.be.a('number');
	            expect(data.total_pages).to.be.a('number');
	            expect(data.per_page).to.be.a('number');
	            expect(data.page).to.be.a('number');
	            expect(data.docs).to.a('array');

	            done();
		    });
		});
	});
});

describe('Subscription Update', function(){

	describe('PUT /subscription/:id', function(){
		it('should update a Subscription', function(done){
			request(app)
			  .put('/subscription/'+subId)
			  .send({plan_id: planId})
			  .set('Content-Type', 'application/json')
			  .set('Authorization', 'Bearer ' + authToken)
			  .end(function (err, res){
	            if(err){
	              return done(err);
	            }

	            if(res.status !== 200) {
	              var _err = JSON.parse(res.text);
	              return done(new Error(_err.message));
	            }

	            var body = res.body;
	            
	  	        expect(body._id).to.be.a('string');
			  	expect(body.plan_id).to.be.a('Object');
			  	expect(body.user_id).to.be.a('Object');
			  	expect(body.plan_id._id).to.be.a('string');
			  	expect(body.user_id._id).to.be.a('string');
			  	expect(body.plan_id._id).to.be.equal(planId);
			  	expect(body.user_id._id).to.be.equal(userId);
	            done();


			  });
		});		
	});

});

describe('Get a single users Subscription', function(){
	describe('GET /subscription/user/:id',function(){
		it("It should return a single user's Subscription",function(done){
		  request(app)
		    .get('/subscription/user/'+ userId)
		    .set('Content-Type', 'application/json')
		    .set('Authorization', 'Bearer ' + authToken)
		    .end(function(err, res){
			    if(err){
	              return done(err);
	            }

	            if(res.status !== 200) {
	              var _err = JSON.parse(res.text);
	              return done(new Error(_err.message));
	            }

	            var body = res.body;

	  	        expect(body._id).to.be.a('string');
			  	expect(body.plan_id).to.be.a('Object');
			  	expect(body.user_id).to.be.a('Object');
			  	expect(body.plan_id._id).to.be.a('string');
			  	expect(body.user_id._id).to.be.a('string');

	            done();
		    });
		});
	});
});

describe('Get collection of subscription for one plan', function(){
	describe('GET /subscription/plan/:id',function(){
		it("It should return all subscription of a single plan by pagination",function(done){
		  request(app)
		    .get('/subscription/plan/'+ planId)
		    .set('Content-Type', 'application/json')
		    .set('Authorization', 'Bearer ' + authToken)
		    .end(function(err, res){
			    if(err){
	              return done(err);
	            }

	            if(res.status !== 200) {
	              var _err = JSON.parse(res.text);
	              return done(new Error(_err.message));
	            }

	            var data = res.body;

	            expect(data.page).to.be.a('number');
	            expect(data.total_docs).to.be.a('number');
	            expect(data.total_pages).to.be.a('number');
	            expect(data.per_page).to.be.a('number');
	            expect(data.page).to.be.a('number');
	            expect(data.docs).to.a('array');

	            done();
		    });
		});
	});
});

describe('Remove subscription', function () {
  describe('DELETE /subscription/:id', function () {
    it('should delete a created subscription', function (done){
      request(app)
        .delete('/subscription/' + subId)
        .set('Content-Type', 'application/json')
        .set('Authorization', 'Bearer ' + authToken)
        .end(function (err, res){
            if(err){
              return done(err);
            }

            if(res.status !== 200) {
              var _err = JSON.parse(res.text);
              return done(new Error(_err.message));
            }

            var body = res.body;

	  	        expect(body._id).to.be.a('string');
			  	expect(body.plan_id).to.be.a('Object');
			  	expect(body.user_id).to.be.a('Object');
			  	expect(body.plan_id._id).to.be.a('string');
			  	expect(body.user_id._id).to.be.a('string');

            done();
          });
    });
  });
});

describe('Remove Plan', function () {
  describe('DELETE /plans/:id', function () {
    it('should delete the created plan', function (done){
      request(appAuth)
        .delete('/plans/' + planId)
        .set('Content-Type', 'application/json')
        .set('Authorization', 'Bearer ' + authToken)
        .end(function (err, res){
            if(err){
              return done(err);
            }

            if(res.status !== 200) {
              var _err = JSON.parse(res.text);
              return done(new Error(_err.message));
            }

	            var plan = res.body;

	            expect(plan._id).to.be.a('string');
	            expect(plan._id).to.equal(planId);
	            expect(plan.description).to.equal(planFix.description);

            done();
          });
    });
  });
});

describe('Remove User', function () {
  describe('DELETE /users/:id', function () {
    it('should delete the created user', function (done){
      request(appAuth)
        .delete('/users/' + userId)
        .set('Content-Type', 'application/json')
        .set('Authorization', 'Bearer ' + authToken)
        .end(function (err, res){
            if(err){
              return done(err);
            }

            if(res.status !== 200) {
              var _err = JSON.parse(res.text);
              return done(new Error(_err.message));
            }

            var user = res.body;

                expect(user._id).to.be.a('string');
	            expect(user._id).to.equal(userId);
	            expect(user.username).to.equal(userFix.email);

            done();
          });
    });
  });
});

after(function () {
process.exit(0);
});